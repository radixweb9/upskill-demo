#!/bin/bash
# Set uid and gid and start containers
export ID=$UID
export USER=$USER
docker-compose up -d --build
# # Copy env file
docker exec upskill-apache cp .env.example .env
# # Install dependencies
docker exec upskill-apache composer install
# Necessary permissions
#docker exec upskill-apache chmod 755 .
docker exec upskill-apache chmod -Rf 777 ./bootstrap/cache ./storage
# Clear caches
docker exec upskill-apache php artisan cache:clear
docker exec upskill-apache php artisan view:clear
docker exec upskill-apache php artisan config:clear
sleep 5
docker exec upskill-apache php artisan migrate:fresh --seed
docker exec upskill-apache php artisan key:generate
docker exec upskill-apache php artisan passport:install