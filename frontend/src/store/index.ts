import { createStore } from "vuex";
import { config } from "vuex-module-decorators";

import AuthModule from "@/store/modules/AuthModule";
import RememberSearchModule from "@/store/modules/RememberSearchModule";
import BodyModule from "@/store/modules/BodyModule";
import BreadcrumbsModule from "@/store/modules/BreadcrumbsModule";
import ActionButtonsModule from "@/store/modules/ActionButtonsModule";
import ConfigModule from "@/store/modules/ConfigModule";

config.rawError = true;

const store = createStore({
  modules: {
    AuthModule,
    RememberSearchModule,
    BodyModule,
    BreadcrumbsModule,
    ActionButtonsModule,
    ConfigModule,
  },
});

export default store;
