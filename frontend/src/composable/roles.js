//We need to add new element here for new roles with specefic permissions.
export function roleList() {
  return [
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Dashboard",
      permissionskeys: ["Read"],
      permissions: ["readDashboard"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Customer",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createCustomer", "readCustomer", "updateCustomer", "deleteCustomer"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Roles Management",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createRole", "readRole", "updateRole", "deleteRole"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Ink",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createInk", "readInk", "updateInk", "deleteInk"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Press",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createPress", "readPress", "updatePress", "deletePress"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Product Option",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createProductMasterOption", "readProductMasterOption", "updateProductMasterOption", "deleteProductMasterOption"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Template Management",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createTemplate", "readTemplate", "updateTemplate", "deleteTemplate"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Product Weight",
      permissionskeys: ["Create", "Read", "Import"],
      permissions: ["createProductWeight", "readProductWeight", "importProductWeight"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Master Packages",
      permissionskeys: ["Create", "Read", "Update", "Delete", "Duplicate"],
      permissions: ["createMasterPackage", "readMasterPackage", "updateMasterPackage", "deleteMasterPackage", "duplicateMasterPackage"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Shipping Packages",
      permissionskeys: ["Read", "Update", "Import"],
      permissions: ["readShippingPackage", "updateShippingPackage","importShippingPackage"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Product",
      permissionskeys: ["Create", "Read", "Update", "Delete", "View", "Duplicate", "Import", "Import History","Generate SKU", "Assign Option"],
      permissions: ["createProduct", "readProduct", "updateProduct", "deleteProduct", "viewProduct", "duplicateProduct", "importProduct", "importProductHistory", "generateSKUProduct", "assignOptionProduct"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Product SKU",
      permissionskeys: ["Read", "Update", "Export", "Update Shipping Package"],
      permissions: ["readProductSKU", "updateProductSKU", "exportProductSKU", "updateShippingPackage"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Trading Partner",
      permissionskeys: ["Create", "Read", "Update", "Delete", "Product Excel Mapping", "Order Excel Mapping"],
      permissions: ["createTradingPartner", "readTradingPartner", "updateTradingPartner", "deleteTradingPartner", "productExcelMapping", "orderExcelMapping"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Orders",
      permissionskeys: ["Create", "Read", "Pull", "View Details", "View Products", "Download Job Ticket", "Import Order", "Import Order History"],
      permissions: ["createOrder", "readOrder", "pullOrder", "viewOrder", "viewOrderProduct", "downloadTicketOrder", "importOrder", "importOrderHistory"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Order Products",
      permissionskeys: ["Read", "View Details"],
      permissions: ["readOrderProduct", "viewOrder"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Pending Order",
      permissionskeys: [ "Read", "view Details", "View Products"],
      permissions: ["readPendingOrder", "viewPendingOrder", "viewPendingOrderProduct"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Pending Order Products",
      permissionskeys: ["Read", "View Details"],
      permissions: ["readPendingOrderProduct", "viewPendingOrder"]
    },
    {
      is_checked_all: false,
      is_indeterminate: false,
      module: "Order Product Status",
      permissionskeys: ["Create", "Read", "Update", "Delete"],
      permissions: ["createOrderProductStatus", "readOrderProductStatus", "updateOrderProductStatus", "deleteOrderProductStatus"]
    }
  ];
}


