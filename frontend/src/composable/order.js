import { ref, onMounted, onUnmounted } from 'vue'

export function createOrderPoductOptions(data) {
  let productAdditionalInfo = [];
  if (!!data["product_options"]) {
    let showChar = 30;
    let ellipsestext = "...";
    if (typeof data['product_options'] == "string") {
      try { 
        let productOptions = JSON.parse(data['product_options']); 
        productOptions.forEach((options, oKey) => {
          let optionTitleHtml = options["option_title"];
          let optionValueHtml = options["option_value"];
          if ( typeof options["option_title"] != "undefined" || typeof options["option_value"] != "undefined" ) {
            if (options["option_title"].length > showChar) {
              optionTitleHtml = options["option_title"].substr(0, showChar) + ellipsestext;
            }
            if (options["option_value"].length > showChar) {
              optionValueHtml = options["option_value"].substr(0, showChar) + ellipsestext;
            }
            productAdditionalInfo[oKey] = {
              option_title: optionTitleHtml,
              option_value: optionValueHtml,
            };
          }
        });
      } catch(e) {
        //Ignore
      }
    }
  }
  return { productAdditionalInfo }
}

export function createOrderPoductFiles(data) {
  let productFilesInfo = [];
  if (!!data["product_files"]) {
    if (typeof data["product_files"] == "string") {
        try {
          let productFiles = JSON.parse(data["product_files"]);
          productFiles.forEach((files, Fkey) => {
            if (files["savedpreview_large"] != "") {
              let image = process.env.VUE_APP_API_URL+'../../../'+files["savedpreview_large"];
              if (/\s/g.test(files["savedpreview_large"])) {
                image = process.env.VUE_APP_API_URL+'../../../'+files["savedpreview_large"].replace(/\s/g, "%20");
              }
              productFilesInfo[Fkey] = image;
            }
          });
        } catch (e) {
          //Ignore
        }
      }
  }
  return { productFilesInfo }
}

export function prepareClientDetails(data) {
  let clientDetails = null;
  if (data.user_info != null) {
    let clientShortCode = "";
    if (data.user_info.shortcode != null)
      clientShortCode = data.user_info.shortcode;
    clientDetails = clientShortCode + "#" + data.order_id;
  }
  return { clientDetails }
}