//Get login users profile from local storage
export const loggedInUserProfile = () => {
    const userInfo = JSON.parse(window.localStorage.getItem('user_info') || "{}");
    let profileImgPath = (userInfo['image_path'] != "") ? process.env.VUE_APP_API_URL+'../../../user/' + userInfo['image_path'] : "/media/avatars/blank.png";
    return profileImgPath;
}
