import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import store from "@/store";
import { Mutations, Actions } from "@/store/enums/StoreEnums";
import { Mutations as rememberMutation } from "@/store/enums/RememberSearchEnums";
import ApiService from "@/core/services/ApiService";
import JwtService from "@/core/services/JwtService";

import customerRoutes from './customer';
import rolesRoutes from './role';
import errorRoutes from './error';
import globalConstant from "@/core/data/globalConstant.js";
import settingsRoutes from "./settings";
const routes= [
  {
    path: "/",
    redirect: "/dashboard",
    component: () => import("@/layout/Layout.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/views/Dashboard2.vue"),
        meta: {
          title: globalConstant.general.APPTITLE +' | Dashboard',
          auth: true,
        },
      },
    ]
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/404",
  },
]
  .concat(customerRoutes)
  .concat(rolesRoutes)
  .concat(errorRoutes)
  .concat(settingsRoutes)
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  // meta title
  document.title = to.meta && to.meta.title ? to.meta.title : globalConstant.general.APPTITLE;

  // reset config to initial state
  store.commit(Mutations.RESET_LAYOUT_CONFIG);
  store.commit(Mutations.RESET_ACTIONBUTTONS_MUTATION);
  next()
  // Scroll page to top on every route change
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});



export default router;
