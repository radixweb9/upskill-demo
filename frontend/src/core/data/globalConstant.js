export default {
  
  // General constant and values should define in this array
  general: {
    APPTITLE: 'Upskill Demo',
    SEARCH: 'Search',
    RESET: 'Reset',
    SRNO: 'Sr#',
    ACTIONS: 'Actions',
    ADD: 'Add',
    EDIT: 'Edit',
    VIEW: 'View',
    DELETE: 'Delete',
    SAVE: 'Save',
    SAVEANDBACK: 'Save & Back',
    CANCEL: 'Cancel',
    SKU: 'SKU',
    TYPE: 'Type',
    DUPLICATE: 'Duplicate',
    STATUS: 'Status',
    SORTORDER: 'Sort Order',
    HEIGHT: 'Height',
    WIDTH: 'Width',
    FILTER_COLUMNS: 'Filter Columns',
    DATE: 'Date',
    QUANTITY: 'Quantity',
    PRICE: 'PRICE',
    EMAIL: 'Email',
    UPLOADFILEPLACETEXT: 'Drop file here or <em>click to upload</em>',
    COLUMNMAPPING: 'Column Mapping',
    FINISH: 'Finish',
    SELECTTRADINGPARTNER: 'Select Trading Partner',
    SUCCESS: 'Success',
    NEXT: 'Next',
    PREVIOUS: 'Previous',
    DOWNLOAD: 'Download',
    INDEX: 'index',
    DATEANDTIME: 'Date & Time',
    EXCELNAME: 'Excel Name',
    DOWNLOADEXCELFILE: 'Download Excel File',
    PRICE: 'Price',
    DESCRIPTION: 'Description',
    DEFAULT: 'Default',
    MASTER_PACKAGE: 'Master Package',
    NAME: 'Name',
    TITLE: 'Title',
    ACTIVE: 'Active',
    INACTIVE: 'Inactive',
    BACK: 'Back',
    ALERT_CONFIMATION: 'OK',
    ALERT_CANCEL: 'Cancel',
    DELETE_MESSAGE: 'Do you really want to delete this record?',
    ALERT_WARNING: 'Warning',
    BACK: 'Back',
    SELECTPRODUCT: 'Select Product',
    SELECTSIZE: 'Select Size',
    SELECTPRODUCTEXCEL: 'Select Product Excel',
    UPDATE: 'Update',
    PRODUCTREQUIRED: 'Product is required',
    EXCELFILEREQUIRED: 'Excel file is required',
    NOTE: 'Note',
    IMAGE: 'Image',
    DOWNLOAD_FILE: 'Download File',
    UPLOAD_FILE: 'Upload File',
    NOTIFY_SUCCESS: 'success',
    NOTIFY_ERROR: 'error',
    NOTIFY_WRONG_MESSAGE: 'Something went wrong',
    NO_DATA_FOUND: 'No Data Found!',
    DUPLICATE_MESSAGE: 'Do you really want to duplicate this record?',
    FOR_SIZE: 'For Size',
    FOR_SIZE_OPTION: 'For Size With Additional Option',
    FOR_SIZE_OPTION_COMBINATION: 'Size with Additional Option Combination',
    PRODUCTS: 'Products',
    ORDERS: 'Orders',
    IS_DEFAULT: 'Is Default',
  },

  // Shipping package constant and values should define in this array
  package: {
    //Master Packages
    PACKAGE_NAME: 'Package Name',
    WEIGHT_UNIT: 'Weight Unit',
    LENGTH_UNIT: 'Length Unit',
    MAX_WEIGHT: 'Max Single Package Weight',
    BOX_WEIGHT: 'Box Weight',
    LENGTH: 'Length',
    PACKAGE_TYPE: 'Package Type',
    INTERNAL: 'Internal',
    EXTERNAL: 'External',
    CHOOSE_WEIGHT: 'Choose Weight Unit',
    CHOOSE_LENGTH: 'Choose Length Unit',
    PACKAGE_DETAILS: 'Package Dimensions',
    MAX_WEIGHT_DETAILS: 'Max Weight',
    BOX_WEIGHT_DETAILS: 'Box Weight',
    MASTER_PACKAGE_PLACEHOLDER: 'Search Master Packages',

    //Shipping Packages
    SHIPPING_PACKAGE_PLACEHOLDER: 'Search by SKU',
    MANAGE: 'Manage Shipping Package',
    INTERNAL_PACKAGE: 'Internal Package',
    EXTERNAL_PACKAGE: 'External Package',
    SELECT_INTERNAL_PACKAGE: 'Select Internal Package',
    SELECT_EXTERNAL_PACKAGE: 'Select External Package',
    IMPORT_SHIPPING_PACKAGES: 'Import Packages',

    //Validation messages
    PACKAGE_NAME_REQUIRED: 'Package name is required',
    MAX_WEIGHT_REQUIRED: 'Max Single Package Weight is required',
    BOX_WEIGHT_REQUIRED: 'Box Weight is required',
    LENGTH_REQUIRED: 'Length is required',
    HEIGHT_REQUIRED: 'Height is required',
    WIDTH_REQUIRED: 'Width is required',
    UNIQUE_PACKAGE_REQUIRED: 'Please enter unique Master Package name',
    EXTERNAL_PACKAGE_REQUIRED: 'External Package is required',

    //Import Packages
    FILL_DATA_HELP: 'Fill up data & help',
    EXCEL_COLUMN: 'Excel Column',
    REQUIRED: 'Required',
    UNIQUE: 'Unique',
    YES: 'Yes',
    NO: 'No',
    SELECT_PACKAGE_EXCEL: 'Select Product Shipping Packages Excel',
    PACKAGEIMPORTMESSAGE: 'Shipping Package Records Updated Successfully',
    PACKAGEIMPORTREDIRECTBTN: 'Go To Shipping Packages',
    IMPORT_PACKAGE_HELP: 'Note: If master package name is not available in system, then system will add master packages from excel sheet and assign them to system SKU. If master package name is available in system, then system will only assign them to system SKU.'
  },

  // General constant and values should define in this array
  tradingPartner: {
    ORDERMAPPING: 'Import Order Excel Columns Mapping',
    PRODUCTMAPPING: 'Import Product Excel Fields Mapping',
    SELECTPLATFORM: 'Select Trading Platform',
    TRADINGPARTNERNAME: 'Trading Partner Name',
    WEBSITEURL: 'Website URL',
    SHORTCODE: 'Shortcode',
    GRANTTYPE: 'Grant Type',
    CLIENTID: 'Client ID',
    SECRETID: 'Secret ID',
    ADDEXTRAFIELD: 'Add Extra Field',
    PULLTYPE: 'Type',
    KEYNAME: 'Key Name',
    KEYVALUE: 'Key Value',
    TYPEREQUIRED: 'Type is required',
    KEYNAMEREQUIRED: 'Key name is required',
    KEYVALUEREQUIRED: 'Key value is required',
    MAPPINGFIELD: 'Product Mapping',
    EXCELHEADERCOLUMN: 'Excel Header Column Name',
    ADDAPIPERAMETERBTN: 'Add API Parameter',
    DEFAULT_ADDRESS: 'Default Return Address',
    RETURN_ADDRESS: 'Return Address',
    NAME: 'Name',
    COMPANY: 'Company',
    ADDRESS1: 'Address1',
    ADDRESS2: 'Address2',
    CITY: 'City',
    STATE: 'State',
    COUNTRY: 'Country',
    PHONE: 'Phone Number',
    POSTAL_CODE: 'Postal Code'
  },


  // Option master constant and values should define in this array
  optionMaster: {
      OPTIONNAME: 'Name',
      OPTIONDESC: 'Description',
      OPTIONSORTORDER: 'Sort Order',
      OPTIONSTATUS: 'Status',
      ATTRIBUTENAME: 'Attribute Name',
      ATTRIBUTESKUCODE: 'Attribute SKU',
      ATTRIBUTEISDEFAULT: 'Is Default',
      ADDMOREATTRIBUTE: 'Add More Attribute',
      ATTRIBUTENAMEREQUIRED: 'Attribute name is required',
      ATTRIBUTECODEREQUIRED: 'Attribute SKU is required',
      ATTRIBUTESKUHELP: 'Attribute SKU will be added after the product and size SKU. (e.g: SXX | SBK).',
      ATTRIBUTE: 'Add Attribute'
  },

   // Orders constant and values should define in this array
  orders: {
    // Order Listing
    ID: 'ID',
    INVOICE_NO: 'Invoice No',
    PLATFORM: 'Platform',
    PARTNER: 'Partner',
    CUSTOMER_DETAILS: 'Customer Details',
    ORDER_DATE:   'Order Date',
    ORDER_DUE_DATE: 'Order Due Date',
    PRODUCT_ITEMS: 'Product Items',
    PAYMENT_AMOUNT: 'Payment Amount',
    PAYMENT_AND_SHIPPING: 'Payment & Shipping',
    ORDER_STATUS: 'Order Status',
    SEARCH_ORDERS: 'Search Orders',
    SELECT_PLATFORM: 'Select Platform',
    PULL_ORDERS: 'Pull Orders',
    IMPORT_ORDERS: 'Import Orders',
    PULL_ORDER_OPTIONS: 'Pull Order Options',
    IMPORT_ORDER_HISTORY: 'Import Order History',
    SHIPPING_CHARGES: 'Shipping Charges',
    SUBTOTAL: 'Subtotal',
    TAX: 'Tax',
    TOTAL_AMOUNT: 'Total Amount',
    DOWNLOAD_JOB_TICKET: 'Download Job Ticket',
    VIEWORDERDETAILS: 'View Order Details',
    VIEWORDERPRODUCTS: 'View Order Items',

    // Order Details
    ORDER_DETAILS: 'Order Details',
    PAYMENT_STATUS: 'Payment Status',
    ORDER_AMOUNT: 'Order Amount',
    CUSTOMER: 'Customer',
    EMAIL: 'Email',
    PHONE: 'Phone',
    COMPANY: 'Company',
    PAYMENT_METHOD: 'Payment method',
    PAYMENT_DATE: 'Payment Date',
    TRANSACTION_ID: 'Transaction Id',
    SHIPPING_METHOD: 'Shipping Method',
    SHIPPING_TYPE: 'Shipping Type',
    BILLING_ADDRESS: 'Billing Address',
    SHIPPING_ADDRESS: 'Shipping Address',
    PHONE_NUMBER: 'Phone Number',
    BLIND_SHIPPING_ADDRESS: 'Blind Shipping Address',
    ORDER_ITEMS: 'Order Items',
    PRODUCT_DETAILS: 'Product Details',
    WEIGHT: 'Weight',
    ADDITIONAL_INFORMATION: 'Additional Information',
    PRODUCTION_DUE: 'Production Due',
    ORDER_EXTRA_FIELDS: 'Order Extra Fields',
    ORDER_ID: "Order Id",

    //Job ticket
    DOWNLOAD_JOB_TICKET_BTN: 'Download Job Tickets',
    DOWNLOAD_ALL_JOB_TICKET_BTN: 'Download All Job Tickets',
    DOWNLOAD_SELECTED_JOB_TICKET_BTN: 'Download Selected Orders Job Ticket'
  },
  
  // Product import constant and values should define in this array
  productImport: {
    PRODUCTIMPORT: 'Import Products',
    PRODUCTSTATISTICS: 'Products Statistics',
    PRODUCTEXCELFILE: 'Select Product Excel',
    SYSTEM_COLUMNS: 'System Columns',
    EXCELCOLUMNS: 'Excel Columns',
    TOTALPRODUCTFOUND: 'Total Found Products',
    NEWPRODUCTS: 'New Products',
    EXISTINGPRODUCTS: 'Existing Products',
    PRODUCTSBTN: 'Go To Products',
    PRODUCTHISTORYBTN: 'Go To Product Import History',
    SAMPLEPRODUCTFILE: 'Sample Product File Download',
    HISTORYBACKBTN: 'Back To History Page',
    SEARCHPRODUCT: 'Search By Product Name',
    SAVE_COLUMN : "Save this column mapping for future imports for the selected trading partner",
    SELECT_MAPPING_COLUMN: 'Select Mapping Column'
  },
  
  // Order Import constant and values should define in this array
  orderImport: {
    ORDERSIMPORT: 'Import Orders',
    UPLOADORDEREXCEL: 'Upload Order Excel File',
    ORDERSUCCESSMSG: 'Order Import Process Completed Successfully',
    PENDINGORDERFOUND: 'Pending Orders Found',
    PENDINGORDERPRODUCTFOUND: 'Pending Order Products Found',
    PENDINGORDERBTN: 'Go To Pending Orders Page',
    NEWORDERFOUND: 'New Orders Found',
    NEWORDERPRODUCTFOUND: 'New Order Products Found',
    NEWORDERBTN: 'Go To Orders Page',
    EXISTORDERFOUND: 'Exist Orders Found',
    EXISTORDERPRODUCTFOUND: 'Exist Order Products Found',
    SAMPLEORDERFILE: 'Sample Order File Download',
    SAVE_COLUMN: 'Save this column mapping for future imports for the selected trading partner',
    SELECT_ORDER_MAPPING: 'Select Order Column Mapping'
  },
  
  // Product constant and values should define in this array
  product: {
    PRODUCT_NAME: 'Product Name',
    SIZE: 'Size',
    PRODUCT_SIZES: 'Product Sizes',
    PRODUCT_SKU: 'Product SKU',
    SIZE_SKU: 'Size SKU',
    IMPORT_PRODUCT: 'Import Products',
    IMPORT_PRODUCT_HISTORY: 'Import Products History',
    SAVEANDOPTION: 'Save & Assign Product Options',
    PRODUCT_DETAILS: 'Product Details',
    SIZE_DETAILS: 'Size Details',
    SKU_DETAILS: 'SKU Details',
    OPTION_DETAILS: 'Option Details',
    WEIGHT_DETAILS: 'Weight Details',
    SHIPPING_PACKAGES: 'Shipping Packages',
    PRODUCT_SKU_HELP: '<span>SKU that will be the first characters<br> of the System Product SKU (e.g: CAN).</span>',
    SIZE_SKU_HELP: '<span>SKU that will be added after the Product<br> SKU to generate System Product SKU (e.g: 11X11).</span>',
    ASSIGN_PRODUCT_OPTIONS: 'Assign Product Options',
    GENERATE_SKU: 'Generate SKU',
    NO_OPTION_FOUND: 'No Product Options Found',
    ATTRIBUTE: 'Attribute',
    ADD_SIZE: 'Add Size',
    MODIFY_SKU_HELP: '<span>This SKU will replace System SKU and System will<br> consider Modify SKU for all future operations.</span>',
    PRODUCT_OPTIONS: 'Product Options',
    PARTNER_SKU: 'Partner SKU', 
    PRODUCT_GENERATE_SKU_HELP: '<span>If this option is disabled then the base SKU will not be generated.</span>',

    //Validation messages
    TITLE_REQUIRED: 'Title is required',
    HEIGHT_REQUIRED: 'Height is required',
    WIDTH_REQUIRED: 'Width is required',
    SIZE_SKU_REQUIRED: 'Size SKU is required',
    PRODUCT_NAME_REQUIRED: 'Product name is required',
    PRODUCT_SKU_REQUIRED: 'Product SKU is required'
  },

  // Presses constant and values should define in this array
  presses: {
    // Listing
    SEARCH_PRESSES: 'Search Presses',
    NAME: 'Name',
    DESCRIPTION: 'Description',
    
    // Add/Edit
    PRESS_NAME: 'Press Name',
    PRESS_DESCRIPTION: 'Description',
  },

  // Inks constant and values should define in this array
  inks: {
    // Listing
    SEARCH_INKS: 'Search Inks',
    NAME: 'Name',
    DESCRIPTION: 'Description',
    
    // Add/Edit
    INK_NAME: 'Ink Name',
    INK_DESCRIPTION: 'Description',
  },

  // Product weight constant and values should define in this array
  weight: {
    PRODUCTWEIGHT: 'Product Weights',
    PRODUCTNAME: 'Product Name',
    SIZE: 'Size',
    PRODUCTOPTION: 'Product Option',
    INDIVIDUALWEIGHT: 'Individual Weight',
    DEFINEWEIGHT: 'Select Options to filter SKUs',
    IMPORTWEIGHTBTN: 'Import Weight Data',
    WEIGHTIMPORT: 'Weight Import',
    DOWNLOADWEIGHTDATA: 'Download Weight Data',
    WEIGHTIMPORTMESSAGE: 'Weight Records Updated Successfully',
    WEIGHTIMPORTREDIRECTBTN: 'Go To Product Weight',
    WEIGHTEXCELEXPORTED: 'Weight content is exported',
    PRODUCTOPTIONVALIDATION: 'Please select product for import excel'
  },
  
  // Template Management constant and values should define in this array
  templateManagement: {
    SETCONTENTTYPE: 'Set Content Type',
    COMMON: 'Common',
    TRADINGPARTNER: 'Trading Partner',
    TEMPLATENAME: 'Template Name',
    TEMPLATEMANAGEMENTCONTENT: 'Template Content',
    AVAILABLEVARIABLES: 'Available Variables',
    ALERT_MESSAGE: 'Are you sure you want to reset the template content?',
    DEFAULT_CONTENT: 'Reset Content'
  },

  // Product sku constant and values should define in this array
  productSku: {
      MODIFYSKU: 'Modify SKU',
      MODIFYWEIGHT: 'Modify Weight',
      MODIFYWEIGHT: 'Modify Weight',
      EXPORTSKU: 'Export SKUs',
      SEARCHSKUCODE: 'Search By SKU'
  },

  // Customers constant and values should define in this array
  customers: {
    // Listing
    NAME: 'Name',
    EMAIL: 'Email',
    ROLES: 'Roles',
    SEARCH_CUSTOMER_DETAILS: 'Search Customer Details',
  },

  orderProductStatus: {
    SEARCH_TITLE: 'Search Title',
    CUSTOMER_STATUS: 'Customer Status',
    USE_ON_JOB_BOARD: 'Use on Job Board',
    STATUS_TITLE: 'Status Title',
    SET_AS: 'Set As',
    BADGE: 'Badge',
    HELP: 'Help',
    INTERNAL_STATUS: 'Internal Status'
  },

  dashboard: {
      
  }

}

