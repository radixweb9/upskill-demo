export default {
  //Pagination common configuration
  paginationConfig: {
    "perPage": 10,
    "pageNumber": 1,
    "pageSize": [10, 25, 50, 100]
  }
}