const DocMenuConfig = [
  {
    pages: [
      {
        heading: "customersListing",
        route: "/customers",
        fontIcon: "bi-person-plus",
        allowedPermission: "readCustomer"
      },
    ],
  },
];

export default DocMenuConfig;
