<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create super admin role 
        $role = Role::create(['name' => 'Super Admin', 'guard_name' => 'api']);

        // create super admin user
        $user = \App\Models\User::factory()->create([
            'name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'user_type' => 0,
            'password' => Hash::make('admin@123'),
        ]);
        $user->assignRole($role);
    }
}
