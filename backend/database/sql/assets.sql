-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Jan 01, 2021 at 10:06 AM
-- Server version: 10.5.6-MariaDB-1:10.5.6+maria~focal
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;

TRUNCATE TABLE `quantities`;
--
-- Dumping data for table `quantities`
--
INSERT INTO `quantities` (`quantity`) VALUES
(25),
(100),
(250),
(500),
(1000),
(2500),
(5000),
(7500),
(10000),
(25000);
COMMIT;