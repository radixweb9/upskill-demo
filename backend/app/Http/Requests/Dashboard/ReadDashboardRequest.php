<?php

namespace App\Http\Requests\Dashboard;

use App\Http\Requests\ApiRequest;

class ReadDashboardRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('readDashboard');
    }
}
