<?php

namespace App\Http\Requests\RolesAndAccess;

use App\Http\Requests\ApiRequest;

class ReadRoleRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Gate::allows('readRole') || \Gate::allows('readCustomer')) {
            return true;
        } else {
            return false;
        }
    }
}
