<?php

namespace App\Http\Requests\RolesAndAccess;

use App\Http\Requests\ApiRequest;

class DeleteRoleRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return \Gate::allows('deleteRole');
    }
}