<?php

namespace App\Http\Requests\RolesAndAccess;

use App\Http\Requests\ApiRequest;

class EditRoleRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return \Gate::allows('updateRole');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'role' => 'sometimes|required|string|max:255',
            'permissions' => 'sometimes|required',
        ];
    }

    /**
     * Message for validation rule
     *
     * @return array
     */
    public function messages() {
        return [
            'role.required' => 'The role name is required',
            'permissions.required' => 'The Permission required'
        ];
    }
}