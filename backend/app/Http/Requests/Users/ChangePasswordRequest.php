<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\ApiRequest;

class ChangePasswordRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'oldPassword' => 'required',
            'newPassword' => ['required', 'min:8', 'regex:/^(?=.*[a-zA-Z])(?=.*\d)(?=.*(_|[^\w])).+$/'],
            'confirmPassword' => 'required|same:newPassword'
        ];
    }

    /**
     * Message for validation rule
     *
     * @return array
     */
    public function messages()
    {
        return [
            'oldPassword.required' => 'The current password is required',
            'newPassword.required' => 'The new password is required',
            'newPassword.min' => 'Please choose a new password with at least 8 characters',
            'newPassword.regex' => 'Contain at least one uppercase/lowercase letters, one number and one special char',
            'confirmPassword.required' => 'The confirm password is required',
            'confirmPassword.same' => 'Please enter the same value again',
        ];
    }
}
