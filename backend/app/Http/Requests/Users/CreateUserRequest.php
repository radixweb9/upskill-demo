<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\ApiRequest;
use App\Rules\UniqueCustomer;

class CreateUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255', new UniqueCustomer()],
            'password' => 'required|string|min:8',
            // 'roles' => 'required'
        ];
    }

    /**
     * Message for validation rule
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The name is required',
            'email.required' => 'The email is required',
            'email.email' => 'Please enter a valid email address',
            'email.unique' => 'The email already exist',
            'password.required' => 'The password is required',
            'password.min' => 'Please choose a password with at least 8 characters',
            'roles.required' => 'The role is required'
        ];
    }
}
