<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\ApiRequest;

class DeleteUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
