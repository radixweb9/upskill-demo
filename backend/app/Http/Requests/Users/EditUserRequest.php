<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\ApiRequest;

class EditUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:255',
            'email' => 'unique:users,email,' . $this->user . ',|sometimes|required|string|email|max:255',
            // 'roles' => 'sometimes|required'
        ];
    }

    /**
     * Message for validation rule
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The name is required',
            'email.required' => 'The email is required',
            'email.email' => 'Please enter a valid email address',
            'email.unique' => 'The email already exist',
            'roles.required' => 'The role is required'
        ];
    }
}
