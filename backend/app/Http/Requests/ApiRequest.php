<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use \Illuminate\Http\Response as Res;

class ApiRequest extends FormRequest
{
    /**
     * Fail validation
     * @param $validator
     * @return exception with json format
     */
    public function failedValidation(Validator $validator)
    {
        $response = [
            'status' => 'error',
            'message' => $validator->errors()->all()
        ];
        throw new HttpResponseException(response()->json($response, Res::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Fail authorization
     * @return exception with json format
     */
    public function failedAuthorization()
    {
        $response = [
            'status' => 'error',
            'message' => 'You are not authorized person to access this request'
        ];
        throw new HttpResponseException(response()->json($response, Res::HTTP_FORBIDDEN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
