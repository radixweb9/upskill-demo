<?php

namespace App\Http\Controllers\Api\v1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Response as Res;
use App\Http\Requests\Users\LoginRequest;
use Exception;
use App\Models\User;
use Hash;
use Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AuthController extends ApiController
{
    /**
     * User Login
     *
     * @param  \App\Http\Requests\Users\LoginRequest  $request
     * @return  \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->respondValidationError('User does not exist');
        }
        $user = $request->user();
        $userData = array('id' => $user['id'], 'name' => $user['name'], 'email' => $user['email'], 'image_path' => $user['image_path']);
        $rolesList = array();
        foreach ($user['roles'] as $val) {
            array_push($rolesList, $val['name']);
        }
        $userData['roles'] = implode(",", $rolesList);
        $permissionsList = array();
        foreach ($user['roles'] as $val) {
            foreach ($val['permissions'] as $val1) {
                array_push($permissionsList, $val1['name']);
            }
        }
        $userData['permissions'] = implode(",", $permissionsList);
        $tokenRes = $user->createToken("Personal Access Token");
        $token = $tokenRes->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return $this->respond([
            'status' => 'success',
            'status_code' => Res::HTTP_OK,
            'api_token' => $tokenRes->accessToken,
            'token_type' => 'Bearer',
            'expire_at' => Carbon::parse(
                $tokenRes->token->expires_at
            )->toDateTimeString(),
            'user_info' => $userData,
        ]);
    }

    /**
     * User Logout
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->respondSuccess("Successfully Logged out.");
    }

    /**
     * Generate reset password token and send reset password form link
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function submitForgetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);
        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        Mail::send('emails.forgetPassword', ['token' => $token], function ($message) use ($request) {
            $message->to($request->email);
            $message->subject('Reset Password');
        });
        return $this->respondSuccess("We have e-mailed your password reset link!");
    }

    /**
     * Show reset password form
     *
     * @return response()
     */
    public function showResetPasswordForm($token)
    {
        $tokenVerify = DB::table('password_resets')->where('token', '=', $token)->where('created_at', '>', Carbon::now()->subHours(1))->first();
        if (!empty($tokenVerify)) {
            return view('auth.passwords.reset', ['token' => $token]);
        } else {
            return redirect('/login')->with('message', 'Your token has been expired!');
        }
    }

    /**
     * Change password functionality using reset password form
     *
     * @return response()
     */
    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);
        $updatePassword = DB::table('password_resets')->where(['email' => $request->email, 'token' => $request->token])->first();
        if (!$updatePassword) {
            return back()->withInput()->with('error', 'Invalid token!');
        }
        User::where('email', $request->email)->update(['password' => Hash::make($request->password)]);
        DB::table('password_resets')->where(['email' => $request->email])->delete();
        return redirect('/login')->with('message', 'Your password has been changed!');
    }
}
