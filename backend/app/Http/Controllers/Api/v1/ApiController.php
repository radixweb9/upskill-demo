<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;
use Response;

class ApiController extends Controller
{
    /**
     * @var int
     */
    protected $statusCode = Res::HTTP_OK;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * set the status code.
     * @param [type] $statusCode [description]
     * @return statuscode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Respond.
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Respond Created.
     * @param $data
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreated($message = "Resource created successfully", $data = null)
    {
        $reponse = ['status' => 'success', 'message' => $message];
        if ($data != null) {
            $reponse['data'] = $data;
        }
        return $this->setStatusCode(Res::HTTP_CREATED)->respond($reponse);
    }

    /**
     * respond with validation error.
     * @param $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondValidationError($errors = null)
    {
        return $this->setStatusCode(Res::HTTP_UNPROCESSABLE_ENTITY)->respond([
            'status' => 'error',
            'message' => $errors,
        ]);
    }

    /**
     * respond with success message
     * @param $message
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondSuccess($message = "Request completed successfully.", $data = null)
    {
        $reponse = ['status' => 'success', 'message' => $message];
        if ($data != null) {
            $reponse['data'] = $data;
        }
        return $this->setStatusCode(Res::HTTP_OK)->respond($reponse);
    }

    /**
     * respond with bad request error
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithBadRequest($message = "Bad Request")
    {
        $this->setStatusCode(Res::HTTP_BAD_REQUEST)->respond([
            'status' => 'error',
            'message' => $message,
        ]);;
    }
}