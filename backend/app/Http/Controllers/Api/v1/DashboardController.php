<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\Dashboard\ReadDashboardRequest;
use App\Repositories\Dashboard\DashboardRepositoryInterface;

class DashboardController extends ApiController
{
    protected $repository;

    /**
     * Constructor
     *
     * @param App\Repositories\Dashboard\DashboardRepositoryInterface $repository
     */
    public function __construct(DashboardRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get Platform Wise Order Statistics
     *
     * @param  App\Http\Requests\Dashboard\ReadDashboardRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function platformStatistics(ReadDashboardRequest $request)
    {
        $data = $this->repository->platformStatistics($request);
        if ($data['status']) {
            return $this->respondSuccess($data['msg'], $data['data']);
        } else {
            return $this->respondValidationError($data['msg']);
        }
    }

    /**
     * Get Job Progress Data
     *
     * @param  App\Http\Requests\Dashboard\ReadDashboardRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function jobProgress(ReadDashboardRequest $request)
    {
        $data = $this->repository->jobProgress($request);
        if ($data['status']) {
            return $this->respondSuccess($data['msg'],$data['data']);
        } else {
            return $this->respondValidationError($data['msg']);
        }
    }
}