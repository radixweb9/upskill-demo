<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\ReadUserRequest;
use App\Http\Requests\Users\EditUserRequest;
use App\Http\Requests\Users\DeleteUserRequest;
use App\Http\Requests\Users\ChangePasswordRequest;

use App\Repositories\Users\UserRepositoryInterface;

class UserController extends ApiController
{

    protected $repository;

    /**
     * Constructor
     * @param App\Repositories\Inks\InkRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @var string Default Sort Parameter for User Listings
     */
    protected $defaultSort = 'latest';

    /**
     * @var array Allowed Sort Parameter array
     */
    protected $allowedSortParameters = [
        'latest',
        'oldest',
    ];

    /**
     * Get the user list
     *
     * @param  App\Http\Requests\Users\ReadUserRequest  $request
     * @return  \Illuminate\Http\Response
     */
    public function index(ReadUserRequest $request)
    {
        $params = array();
        $params['user_type'] = $request->user_type;
        $sortBy = $request->sortby;
        if ($sortBy == null) {
            $sortBy = $this->defaultSort;
        }
        $params['sortby'] = $sortBy;
        if (in_array($sortBy, $this->allowedSortParameters)) {
            $users = $this->repository->getAllUsers($params);
            return $this->respondSuccess('Request completed successfully.', $users);
        } else {
            return $this->respondWithBadRequest();
        }
    }

    /**
     * Store an user.
     *
     * @param  App\Http\Requests\Users\CreateUserRequest  $request
     * @return \Illuminate\Http\Response
     * 
     */
    public function store(CreateUserRequest $request)
    {
        $params = array();
        $params['name'] = $request->name;
        $params['email'] = $request->email;
        $params['password'] = $request->password;
        $params['roles'] = $request->roles;
        $user = $this->repository->addUser($params);
        $data['id'] = $user['id'];
        return $this->respondCreated('User created successfully', $data);
    }

    /**
     * Display the specified user by id
     *
     * @param App\Http\Requests\Users\ReadUserRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     */
    public function show(ReadUserRequest $request, $id)
    {
        $user = $this->repository->getUser($id);
        return $this->respondSuccess('Request completed successfully.', $user);
    }

    /**
     * Update the specified user by id
     *
     * @param  App\Http\Requests\Users\EditUserRequest  $request
     * @param  int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update(EditUserRequest $request, $id)
    {
        $request['id'] = $id;
        $this->repository->editUser($request->all());
        return $this->respondSuccess('User updated successfully');
    }

    /**
     * Remove the specified user by id 
     *
     * @param App\Http\Requests\Users\DeleteUserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteUserRequest $request, $id)
    {
        $this->repository->deleteUser($id);
        return $this->respondSuccess('User deleted successfully');
    }

    /**
     * Change password user by id
     * @param  App\Http\Requests\Users\ChangePasswordRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $params = array();
        $params['id'] = $request->userId;
        $params['oldPassword'] = $request->oldPassword;
        $params['newPassword'] = $request->newPassword;
        $params['confirmPassword'] = $request->confirmPassword;
        $password = $this->repository->changePassword($params);
        if ($password['status']) {
            return $this->respondSuccess($password['msg']);
        } else {
            return $this->respondValidationError($password['msg']);
        }
    }

    /**
     * Save filters of user by id
     * @param  App\Http\Requests\Users\EditUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function saveFilters(EditUserRequest $request)
    {
        $key = collect($request->all())->keys()[0] ?? '';
        $filters = [];
        if (isset($request[$key]) && !empty($request[$key])) {
            $filters[$key] = call_user_func_array('array_merge', $request[$key]);
        }
        $params = array();
        $params['id'] = \Auth::user()->id;
        $params['filter'] = $filters;
        $result = $this->repository->saveFilters($params);
        if ($result) {
            return $this->respondSuccess('Filters saved successfully');
        } else {
            return $this->respondValidationError("Something went wrong while saving filters");
        }
    }

    /**
     * Get the specified user by id
     *
     * @param App\Http\Requests\Users\ReadUserRequest $request
     * @return \Illuminate\Http\Response
     * 
     */
    public function getLoggedInUserProfileData(ReadUserRequest $request)
    {
        $user = $this->repository->getUser(\Auth::user()->id);
        return $this->respondSuccess('Request completed successfully.', $user);
    }

    /**
     * Update the specified user by id
     *
     * @param  App\Http\Requests\Users\EditUserRequest  $request
     * @return  \Illuminate\Http\Response
     * 
     */
    public function updateUserProfile(EditUserRequest $request)
    {
        $profileUpdate = $this->repository->updateUserProfile($request);
        if ($profileUpdate) {
            return $this->respondSuccess("Profile Updated Successfully", $profileUpdate);
        } else {
            return $this->respondValidationError("Profile not updated.");
        }
    }
}
