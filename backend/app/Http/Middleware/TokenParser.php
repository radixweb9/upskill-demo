<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Laravel\Passport\Token;
use Lcobucci\JWT\Configuration;

class TokenParser
{
    /**
     * Handle an incoming request.
     * This method fetch the bearer token from the request then get the client details for that token
     * get UserId for that client and add it into the request params
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // $tokenId = (new Parser())->parse($request->bearerToken())->getClaim('jti');
        $jwtConfig = Configuration::forUnsecuredSigner();
        $tokenId = $jwtConfig->parser()->parse($request->bearerToken())->claims()->get('jti');
        $client = Token::find($tokenId)->client;
        if (isset($client['user_id'])) {
            $request->request->add(['user_id' => $client['user_id']]);
        }
        return $next($request);
    }
}
