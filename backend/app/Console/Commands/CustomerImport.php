<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Customers\CustomerRepository;

class CustomerImport extends Command
{
    protected $repository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customerimport:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the website customers data in manufacture portal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CustomerRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->repository->importWebsiteCustomers();
    }
}
