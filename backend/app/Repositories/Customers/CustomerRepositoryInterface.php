<?php

namespace App\Repositories\Customers;

interface CustomerRepositoryInterface
{
    /**
     * Get all Customers.
     *
     * @return $params
     */
    public function getAllCustomers($params);

    /**
     * Delete an Customer by id
     *
     * @param int
     */
    public function deleteCustomer($id);

    /**
     * Import the website customers into database
     */
    public function importWebsiteCustomers();
}
