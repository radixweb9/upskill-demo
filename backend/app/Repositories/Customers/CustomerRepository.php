<?php

namespace App\Repositories\Customers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use App\Models\Customer;
use App\Models\TradingPartner;

class CustomerRepository implements CustomerRepositoryInterface
{
    /**
     * Get all Customers.
     *
     * @param $params
     * @return collection
     */
    public function getAllCustomers($params)
    {
        $pageNub = $params['pageNub'];
        $recordsPerPage = $params['recordsPerPage'];
        $sortBy = $params['sortby'];
        $searchByStoreID = $params['store_id'];

        $queryCustomer = Customer::query();
        //Apply Sorting
        if ($sortBy == 'latest') {
            $queryCustomer->orderBy('created_at', 'desc');
        } else if ($sortBy == 'oldest') {
            $queryCustomer->orderBy('created_at', 'asc');
        } else {
            $queryCustomer->orderBy($sortBy);
        }
        // Search by store id
        if ($searchByStoreID != null) {
            $queryCustomer->where('store_id', $searchByStoreID);
        }
        if ($recordsPerPage == null || $recordsPerPage == 0) {
            $Customers = $queryCustomer->get();
        } else {
            $Customers = $queryCustomer->paginate($recordsPerPage, ['*'], 'pageNub', $pageNub);
        }
        return $Customers;
    }

    /**
     * Delete a Customer by id
     *
     * @param int $id
     */
    public function deleteCustomer($id)
    {
        Customer::findOrFail($id)->delete();
    }

    /**
     * Import the website customers into database
     * Create user if not exist, otherwise update the existing user info
     */
    public function importWebsiteCustomers()
    {
        $customersLastSyncDate = Cache::get('customers_last_sync_date'); //Get the customers last sync date from the cache
        $query = TradingPartner::query();
        $query->select('trading_partners.user_id', 'trading_partners.store_info');
        $query->join('users', 'users.id', '=', 'trading_partners.user_id');
        $query->where('users.user_type', '=', '1');
        $clientData = $query->get();
        if ($clientData->count() > 0) {
            foreach ($clientData as $value) {
                $storeInfo = $value['store_info'];
                if ($storeInfo != null) {
                    $storeInfo = json_decode($storeInfo, true);
                    $apiUrl = $storeInfo['api_url'] . '?token=' . $storeInfo['access_token'];
                    $requestParams['from_date'] = $customersLastSyncDate;
                    $requestParams['date_type'] = "registration";
                    $customerData = Http::post($apiUrl, [
                        'query' => 'query customers ($email: String, $from_date: String, $to_date: String, $date_type: CustomerDateTypeEnum, $limit: Int, $offset: Int) {
                            customers (email: $email, from_date: $from_date, to_date: $to_date, date_type: $date_type, limit: $limit, offset: $offset) {
                                customers {customers_first_name customers_last_name customers_email_address customers_telephone}
                                totalCustomers
                            }
                        }',
                        'variables' => json_encode($requestParams)
                    ]);
                    if ($customerData->json()['data']['customers'] != null) {
                        $customerData = $customerData->json()['data']['customers']['customers'];
                        foreach ($customerData as $data) {
                            if ($data['customers_first_name'] && $data['customers_last_name'] && $data['customers_email_address']) {
                                Customer::updateOrCreate(
                                    ['store_id' => $value['user_id'], 'email' => $data['customers_email_address']],
                                    ['first_name' => $data['customers_first_name'], 'last_name' => $data['customers_last_name'], 'email' => $data['customers_email_address'], 'phone' => $data['customers_telephone']]
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}
