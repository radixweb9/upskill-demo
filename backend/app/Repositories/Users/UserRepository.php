<?php

namespace App\Repositories\Users;

use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    /**
     * Get an user by it's ID
     *
     * @param int $id
     * @return collection
     */
    public function getUser($id)
    {
        $user = User::findOrFail($id);
        $user['rolelist'] = $user->roles->pluck('name');
        unset($user->roles);
        return $user;
    }

    /**
     * Get all users.
     *
     * @param $params
     * @return collection
     */
    public function getAllUsers($params)
    {
        $sortBy = $params['sortby'];

        $queryUser = User::query();
        //Apply Sorting
        if ($sortBy == 'latest') {
            $queryUser->orderBy('created_at', 'desc');
        } else if ($sortBy == 'oldest') {
            $queryUser->orderBy('created_at', 'asc');
        }
        $users = $queryUser->get();
        return $users;
    }

    /**
     * Update an user by it's ID
     *
     * @param $params
     *
     */
    public function editUser($params)
    {
        $user = User::findOrFail($params['id']);
        if (isset($params['name'])) $user->name = $params['name'];
        if (isset($params['email']))  $user->email = $params['email'];
        if (!empty($params['roles'])) {
            $user->roles()->detach();
            $roles = array();
            $roles = $params['roles'];
            foreach ($roles as $role) {
                $user->assignRole($role);
            }
        }
        $user->save();
    }

    /**
     * Add an user 
     *
     * @param $params
     * @return array $user
     */
    public function addUser($params)
    {
        $user = new User();
        $user->name = $params['name'];
        $user->email = $params['email'];
        if (!empty($params['password'])) {
            $user->password = bcrypt($params['password']);
        } else {
            $user->password = "";
        }
        if (!empty($params['user_type'])) {
            $user->user_type = $params['user_type'];
        }
        $user->save();
        if (!empty($params['roles'])) {
            foreach ($params['roles'] as $role) {
                $user->assignRole($role);
            }
        }
        return $user;
    }

    /**
     * Delete an user by id
     *
     * @param int $id
     */
    public function deleteUser($id)
    {
        User::findOrFail($id)->delete();
    }

    /**
     * Change password by id
     * 
     * @param $params
     * @return boolean 
     * 
     */
    public function changePassword($params)
    {
        $id = $params['id'];
        $user = User::findOrFail($id);
        $oldPassword = $params['oldPassword'];
        $newPassword = $params['newPassword'];
        $dbPassword = $user->password;

        if (Hash::check($oldPassword, $dbPassword)) {
            $user->password = Hash::make($newPassword);
            $user->save();
            return array("status" => true, "msg" => "Your password updated successfully.");
        } else {
            return array("status" => false, "msg" => "Current password and new password are not matching.",);
        }
    }

    /**
     * Save filters of user by id
     * 
     * @param $params
     * @return boolean 
     * 
     */
    public function saveFilters($params)
    {
        $filter = "";
        $user = User::findOrFail($params['id']);
        $filter = $user->filters;
        if (isset($params['filter']) && !empty($params['filter'])) {
            $filter[key($params['filter'])] = $params['filter'][key($params['filter'])];
        }
        $user->filters = $filter;
        return $user->save();
    }

    /**
     * Update an user by it's ID
     *
     * @param $params
     * @return array $user
     * 
     */
    public function updateUserProfile($params)
    {
        $image = $params['file'];
        $user = User::findOrFail($params['id']);
        $image_name = "";
        if (!empty($image)) {
            if ($user->image_path != "" && file_exists(public_path("/user/" . $user->image_path))) {
                unlink(public_path("/user/" . $user->image_path));
            }
            $image_name = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path('/user'), $image_name);
        } else if ($params['old_image_path'] != "") {
            $image_name = $params['old_image_path'];
        }
        $user->image_path = $image_name;
        $user->name = $params['name'];
        $user->save();
        return $user;
    }
}
