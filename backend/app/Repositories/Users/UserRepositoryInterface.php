<?php 

namespace App\Repositories\Users;

interface UserRepositoryInterface
{

    /**
     * Get all users.
     *
     * @return $params
     */
    public function getAllUsers($params);
    
    /**
     * Get an user by it's ID
     *
     * @param int $id
     */
    public function getUser($id);

    /**
     * Update an User by it's ID
     *
     * @param $params
     */
    public function editUser($params);

    /**
     * Add an user
     *
     * @param mix
     * 
     */
    public function addUser($params);

    /**
     * Delete an user by id
     *
     * @param int
     */
    public function deleteUser($id);

    /**
     * Change an User's password by ID
     * 
     * @param $params
     */
    public function changePassword($params);

    /**
     * Save filters of user by id
     * 
     * @param $params
     */
    public function saveFilters($params);
}