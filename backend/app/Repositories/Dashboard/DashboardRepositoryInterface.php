<?php

namespace App\Repositories\Dashboard;

interface DashboardRepositoryInterface
{
    /**
     * Get Platform Wise Order Statistics
     *
     */
    public function platformStatistics();

    /**
     * Get Job Progress Data
     *
     */
    public function jobProgress();
}
