<?php

namespace App\Repositories\Dashboard;

use App\Models\TradingPlatforms;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class DashboardRepository implements DashboardRepositoryInterface
{
    /**
     * Get Platform Wise Order Statistics
     *
     * @return $array
     */
    public function platformStatistics()
    {
        $platformFilterData = array();
        $platformStatisticsData = Order::withCount(['orderProducts'])->with(['tradingPlatforms', 'orderPayment' => function ($query) {
            $query->select('payment_id', 'order_id', DB::raw("SUM(total_amount) as total"));
        }])->orderBy('platform')->get()->toArray();
        if (!empty($platformStatisticsData)) {
            $i = $orderProductCount = $totalProductSales = 0;
            $platformId = '';
            foreach ($platformStatisticsData as $key => $value) {
                if ($platformId == '') {
                    $platformId = $value['platform'];
                }
                if ($platformId == $value['platform']) {
                    $platformFilterData[$i]['platform_name'] = $value['trading_platforms']['platform_name'];
                    $platformFilterData[$i]['order_product_count'] = $orderProductCount += $value['order_products_count'];
                    $platformFilterData[$i]['total_product_sale_amount'] = !empty($value['order_payment']) ? $totalProductSales += $value['order_payment']['total'] : $totalProductSales;
                } else {
                    $i++;
                    $platformFilterData[$i]['platform_name'] = $value['trading_platforms']['platform_name'];
                    $platformFilterData[$i]['order_product_count'] = $orderProductCount += $value['order_products_count'];
                    $platformFilterData[$i]['total_product_sale_amount'] = !empty($value['order_payment']) ? $totalProductSales += $value['order_payment']['total'] : $totalProductSales;
                }
                $platformId = $value['platform'];
            }
            return array('status' => true, 'data' => $platformFilterData, 'msg' => "Request completed successfully.");
        } else {
            return array('status' => false, 'data' => null, 'msg' => "No data found.");
        }
    }

    /**
     * Get Job Progress Data
     *
     * @return $array
     */
    public function jobProgress()
    {
        $jobProgressFilterData = array();
        $jobProgressData = Order::get();
        if (!empty($jobProgressData)) {
            foreach ($jobProgressData as $key => $value) {
                if ($value->order_due_date <= date('Y-m-d', strtotime('-' . 1 . ' day', time()))) {
                    $jobProgressFilterData['oldData'][$key] = array('order_id' => $value->order_id);
                    $jobProgressFilterData['currentData'][$key] = array('order_id' => $value->order_id);
                } else if ($value->order_due_date == date('Y-m-d')) {
                    $jobProgressFilterData[0]['currentData'][$key] = array('order_id' => $value->order_id);
                } else if ($value->order_due_date == date('Y-m-d', strtotime('+' . 1 . ' day', time()))) {
                    $jobProgressFilterData[0]['futureData'][$key] = array('order_id' => $value->order_id);
                }
            }
            return array('status' => true, 'data' => $jobProgressFilterData, 'msg' => "Request completed successfully.");
        } else {
            return array('status' => false, 'data' => null, 'msg' => "No data found.");
        }
    }
}
