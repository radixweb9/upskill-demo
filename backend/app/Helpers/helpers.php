<?php

use Illuminate\Support\Str;
use App\Models\ProductMasterOptionAttributes;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductSku;
use App\Models\TempOrder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

/**
 * Get Weight class(unit) for Package configuration
 *
 * @return array
 */
function getWeightClass()
{
    $weightArr = array("LB" => "LBS", "KG" => "Kilogram");
    return $weightArr;
}

/**
 * Get Length class(unit) for Package configuration
 *
 * @return array
 */
function getLengthClass()
{
    $lengthArr = array("IN" => "Inch", "CM" => "Centimeter");
    return $lengthArr;
}

/**
 * Get Shippign method list
 *
 * @return array
 */
function getShippingMethod()
{
    $shippingArr = array(1 => "UPS", 2 => "Fedex");
    return $shippingArr;
}

/**
 * Remove space from array key and add underscore
 * @param array &$arr
 * @return array
 * 
 */
function fixArrayKey(&$arr)
{
    $arr = array_combine(
        array_map(
            function ($str) {
                return strtolower(str_replace(" ", "_", $str));
            },
            array_keys($arr)
        ),
        array_values($arr)
    );

    foreach ($arr as $key => $val) {
        if (is_array($val)) {
            fixArrayKey($arr[$key]);
        }
    }
    return $arr;
}

/**
 * Download and save external files to provided path
 * 
 * @param  string  $pathToSaveFile
 * @param  string  $fileUrl
 * @return string  $file
 */
if (!function_exists('downloadFile')) {
    function downloadFile($pathToSaveFile, $fileUrl)
    {
        // Get file name by the file url
        $fileName = substr($fileUrl, strrpos($fileUrl, '/') + 1);

        // save file to the path to save
        $file = $pathToSaveFile . '/' . $fileName;

        if (!File::exists(public_path() . $pathToSaveFile . '/' . $fileName)) {
            // Initiate curl
            $curl = curl_init();
            // Curl options
            curl_setopt($curl, CURLOPT_URL, $fileUrl);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            // Curl execution and fetching http code
            $data = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // Close curl connection
            curl_close($curl);
            // Check if return code is 200
            if ($httpCode == 200) {
                // Check if path to save file is already exist
                if (!File::exists(public_path() . $pathToSaveFile)) {
                    // Create path to save file if not exist
                    File::makeDirectory(public_path() . $pathToSaveFile, 0777, true);
                }

                File::put(public_path() . $file, $data);
                return $file;
            } else {
                return null;
            }
        } else {
            return $file;
        }
    }
}

/**
 * This function search in associative array
 * 
 * @param  array  $array
 * @param  array  $search_list
 * @return array  $result
 */
function search($array, $search_list)
{
    // Create the result array
    $result = array();
    // Iterate over each array element
    foreach ($array as $key => $value) {
        // Iterate over each search condition
        foreach ($search_list as $k => $v) {
            // If the array element does not meet
            // the search condition then continue
            // to the next element
            if (!isset($value[$k]) || $value[$k] != $v) {
                // Skip two loops
                continue 2;
            }
        }
        // Append array element's key to the
        //result array
        $result[] = $value;
    }
    // Return result 
    return $result;
}


/**
 * This function replace the curly variable to actual value
 * 
 * @param  string  $mainString
 * @param  array  $replaceVariable
 * @return string  $mainString
 */
function replaceCurlyVariable($mainString , $replaceVariable){
   return preg_replace_callback('/{(.+?)}/ix',function($match)use($replaceVariable){
        return !empty($replaceVariable[$match[1]]) ? $replaceVariable[$match[1]] : $match[0];
    }, $mainString);
}

/**
 * Fetch additional details based on sku code
 * 
 * @param string $code
 * @return array
 */
function getAdditionalCodes($code)
{
    $productAdditionalPrepareData = array();
    $firstAttribute = Str::of($code)->substr(0, 1);
    $secondAttribute = Str::of($code)->substr(1, 2);
    $findFirstAttributeSkuCode = ProductMasterOptionAttributes::select('attribute_name')->where('sku_code', $firstAttribute)->first();
    $findSecondAttributeSkuCode = ProductMasterOptionAttributes::select('attribute_name')->where('sku_code', $secondAttribute)->first();
    if (!empty($findFirstAttributeSkuCode) && !empty($findSecondAttributeSkuCode)) {
        $productAdditionalPrepareData[0] = $findFirstAttributeSkuCode->toArray();
        $productAdditionalPrepareData[1] = $findSecondAttributeSkuCode->toArray();
    }
    return $productAdditionalPrepareData;
}

/**
 * Fetch order, product, sku count
 * 
 * @return array @$dashboardStatistics
 */
function dashboardStatistics()
{
    $dashboardStatistics = array();
    $dashboardStatistics['orderCount'] = Order::count();
    $dashboardStatistics['productCount'] = Product::where('status','1')->count();
    $dashboardStatistics['skuCount'] = ProductSku::count();
    return $dashboardStatistics;
}

/**
 * Update trading platform id with platform name for orders
 * 
 * @return string
 */
function updateTradingPlatformID()
{
    DB::beginTransaction();
    try {
        $tempOrders = TempOrder::where(function ($q) {
            $q->where('platform', 'creative gallery')
                ->orWhere('platform', 'photography')
                ->orWhere('platform', 'amazon');
        })->get();
        if (!empty($tempOrders)) {
            foreach ($tempOrders as $tempOrder) {
                if ($tempOrder->platform == "creative gallery") {
                    $tempOrder->platform = '2';
                } else if ($tempOrder->platform == "photography") {
                    $tempOrder->platform = '4';
                } else if ($tempOrder->platform == "amazon") {
                    $tempOrder->platform = '6';
                }
                $tempOrder->save();
            }
        }
        $orders = Order::where(function ($q) {
            $q->where('platform', 'creative gallery')
                ->orWhere('platform', 'photography')
                ->orWhere('platform', 'amazon');
        })->get();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                if ($order->platform == "creative gallery") {
                    $order->platform = '2';
                } else if ($order->platform == "photography") {
                    $order->platform = '4';
                } else if ($order->platform == "amazon") {
                    $order->platform = '6';
                }
                $order->save();
            }
        }
        DB::commit();
        echo "Trading platform updated successfully.";
    } catch (\Exception $e) {
        DB::rollback();
        echo "Error while updating trading platform.";
    }
}

/**
 * Convert space into undersocre for array
 * 
 * @return array
 */
function spaceIntoUnderscore($val) {
    return str_replace(" ", "_", $val);
}

