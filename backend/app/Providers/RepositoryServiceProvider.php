<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Dashboard\DashboardRepositoryInterface',
            'App\Repositories\Dashboard\DashboardRepository'
        );
        $this->app->bind(
            'App\Repositories\Presses\PressRepositoryInterface',
            'App\Repositories\Presses\PressRepository'
        );
        $this->app->bind(
            'App\Repositories\Inks\InkRepositoryInterface',
            'App\Repositories\Inks\InkRepository'
        );
        $this->app->bind(
            'App\Repositories\Users\UserRepositoryInterface',
            'App\Repositories\Users\UserRepository'
        );
        $this->app->bind(
            'App\Repositories\Roles\RoleRepositoryInterface',
            'App\Repositories\Roles\RoleRepository'
        );
        $this->app->bind(
            'App\Repositories\Assets\AssetRepositoryInterface',
            'App\Repositories\Assets\AssetRepository'
        );
        $this->app->bind(
            'App\Repositories\PriceGroup\PriceGroupRepositoryInterface',
            'App\Repositories\PriceGroup\PriceGroupRepository'
        );
        $this->app->bind(
            'App\Repositories\Orders\OrderRepositoryInterface',
            'App\Repositories\Orders\OrderRepository'
        );
        $this->app->bind(
            'App\Repositories\Orders\TempOrderRepositoryInterface',
            'App\Repositories\Orders\TempOrderRepository'
        );
        $this->app->bind(
            'App\Repositories\OrderProductStatus\OrderProductStatusRepositoryInterface',
            'App\Repositories\OrderProductStatus\OrderProductStatusRepository'
        );
        $this->app->bind(
            'App\Repositories\OrderProductStatus\OrderProductStatusRepositoryInterface',
            'App\Repositories\OrderProductStatus\OrderProductStatusRepository'
        );
        $this->app->bind(
            'App\Repositories\TemplateManagement\TemplateManagementRepositoryInterface',
            'App\Repositories\TemplateManagement\TemplateManagementRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductMasterOption\ProductMasterOptionRepositoryInterface',
            'App\Repositories\ProductMasterOption\ProductMasterOptionRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductMasterOptionAttribute\ProductMasterOptionAttributeRepositoryInterface',
            'App\Repositories\ProductMasterOptionAttribute\ProductMasterOptionAttributeRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductWeight\ProductWeightRepositoryInterface',
            'App\Repositories\ProductWeight\ProductWeightRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductSku\ProductSkuRepositoryInterface',
            'App\Repositories\ProductSku\ProductSkuRepository'
        );
        $this->app->bind(
            'App\Repositories\AssetRules\AssetRuleRepositoryInterface',
            'App\Repositories\AssetRules\AssetRuleRepository'
        );
        $this->app->bind(
            'App\Repositories\PredefinedComments\PredefinedCommentsRepositoryInterface',
            'App\Repositories\PredefinedComments\PredefinedCommentsRepository'
        );
        $this->app->bind(
            'App\Repositories\Turnaround\TurnaroundRepositoryInterface',
            'App\Repositories\Turnaround\TurnaroundRepository'
        );
        $this->app->bind(
            'App\Repositories\AssetGroup\AssetGroupRepositoryInterface',
            'App\Repositories\AssetGroup\AssetGroupRepository'
        );
        $this->app->bind(
            'App\Repositories\Quote\QuoteInterface',
            'App\Repositories\Quote\QuoteRepository'
        );
        $this->app->bind(
            'App\Repositories\TradingPartner\TradingPartnerInterface',
            'App\Repositories\TradingPartner\TradingPartnerRepository'
        );
        $this->app->bind(
            'App\Repositories\Products\ProductRepositoryInterface',
            'App\Repositories\Products\ProductRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductImportHistory\ProductImportHistoryRepositoryInterface',
            'App\Repositories\ProductImportHistory\ProductImportHistoryRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductAssignAdditionalOption\ProductAssignAdditionalOptionRepositoryInterface',
            'App\Repositories\ProductAssignAdditionalOption\ProductAssignAdditionalOptionRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductShippingPackage\ProductShippingPackageRepositoryInterface',
            'App\Repositories\ProductShippingPackage\ProductShippingPackageRepository'
        );
        $this->app->bind(
            'App\Repositories\ProductMasterPackage\ProductMasterPackageRepositoryInterface',
            'App\Repositories\ProductMasterPackage\ProductMasterPackageRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
