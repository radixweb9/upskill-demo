<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Product;

class UniqueProductSku implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($skucode, $id = '')
    {
        $this->productId = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $product = Product::where('sku_code', $value)->where('product_id', '!=', $this->productId)->first();
        if (!empty($product)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sku code is already exist.';
    }
}
