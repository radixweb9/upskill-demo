<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ProductMasterOptionAttributes;

class UniqueOptionAttributeSku implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($skucode, $id = '')
    {
        $this->masterOptionId = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $productMasterOptionAttributes = ProductMasterOptionAttributes::where('sku_code', $value)->where('master_option_id', '!=', $this->masterOptionId)->first();
        if (!empty($productMasterOptionAttributes)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Attribute sku code is already exist.';
    }
}
