<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UniqueTradingPartner implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($email, $userID = '')
    {
        $this->email = $email;
        $this->userID = $userID;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('email', $value)->where('user_type', '2')->where('id', '!=', $this->userID)->first();
        if (!empty($user)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email address already exist.';
    }
}
